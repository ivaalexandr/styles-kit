const { src, dest, series } = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const ts = require('gulp-typescript');

function buildStyles(cb) {
  src('./src/**/*.scss')
    .pipe(dest('./GeoStyles', { overwrite: true }))
    .pipe(sass().on('error', sass.logError))
    .pipe(dest('./GeoStyles', { overwrite: true }));
  cb();
}

function buildJs(cb) {
  const tsProject = ts.createProject('./tsconfig.json');
  src('./src/**/*.ts')
    .pipe(tsProject())
    .pipe(dest('./GeoStyles'));
    cb();
}

exports.build = series(buildStyles, buildJs);