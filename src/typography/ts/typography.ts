export interface PropertyInterface {
  fontFamily?: string;
  fontStyle?: string;
  fontWeight?: string | number;
  fontSize?: string;
  lineHeight?: string;
  letterSpacing?: string;
  color?: string;
  textTransform?: 'none' | 'capitalize' | 'uppercase' | 'lowercase' | 'full-width' | 'full-size-kana';
}

export interface TypographyInterface {
  H1: PropertyInterface;
  H2: PropertyInterface;
  H3: PropertyInterface;
  H4: PropertyInterface;
  title1: PropertyInterface;
  title2: PropertyInterface;
  body1: PropertyInterface;
  body2: PropertyInterface;
  description: PropertyInterface;
  subtitle1: PropertyInterface;
  subtitle2: PropertyInterface;
  subtitle3: PropertyInterface;
  accentTitle1: PropertyInterface;
  accentTitle2: PropertyInterface;
  button: PropertyInterface;
  headmenu: PropertyInterface;
  MuiChip: PropertyInterface;
}

export const typography: TypographyInterface = {
  H1: {
    fontFamily: "Blender Pro",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "6.4rem",
    lineHeight: "6.4rem",
    letterSpacing: "-0.42rem",
  },

  H2: {
    fontFamily: "Blender Pro",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "4rem",
    lineHeight: "4rem",
    letterSpacing: "-0.03rem",
  },

  H3: {
    fontFamily: "Blender Pro",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "2.2rem",
    lineHeight: "2rem",
    letterSpacing: "-0.02rem",
    textTransform: "uppercase",
  },

  H4: {
    fontFamily: "Blender Pro",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "2.2rem",
    lineHeight: "2rem",
    letterSpacing: "-0.02rem",
    textTransform: "uppercase",
  },

  title1: {
    fontFamily: "Inter",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "2rem",
    lineHeight: "3.2rem",
    letterSpacing: "-0.05rem",
  },

  title2: {
    fontFamily: "Inter",
    fontStyle: "normal",
    fontWeight: 500,
    fontSize: "1.2rem",
    lineHeight: "1.6rem",
  },

  subtitle1: {
    fontFamily: "Inter",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "1.6rem",
    lineHeight: "2rem",
    color: "#000000",
  },

  subtitle2: {
    fontFamily: "Inter",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "1.2rem",
    lineHeight: "1.5rem",
    color: "#000000",
  },

  subtitle3: {
    fontFamily: "Inter",
    fontStyle: "normal",
    fontWeight: 500,
    fontSize: "1rem",
    lineHeight: "1.6rem",
    color: "#000000",
  },

  body1: {
    fontFamily: "Inter",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "1.4rem",
    letterSpacing: "-0.02rem",
  },

  body2: {
    fontFamily: "Inter",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "1.2rem",
    lineHeight: "1.6rem",
    letterSpacing: "-0.02rem",
  },

  accentTitle1: {
    fontFamily: "Blender Pro",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "1.6rem",
    lineHeight: "1.4rem",
    letterSpacing: "-0.02rem",
    color: "#000000",
  },

  accentTitle2: {
    fontFamily: "Blender Pro",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "1.2rem",
    lineHeight: "1.2rem",
    letterSpacing: "-0.02rem",
    color: "#000000",
  },

  button: {
    fontFamily: "Inter",
    fontStyle: "normal",
    fontWeight: 500,
    fontSize: "1.4rem",
    lineHeight: "2.4rem",
    letterSpacing: "0.02rem",
    color: "#000000",
    textTransform: "none",
  },

  description: {
    fontFamily: "Inter",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "1rem",
    lineHeight: "1.2rem",
  },

  headmenu: {
    fontFamily: "Inter",
    fontStyle: "normal",
    fontWeight: 500,
    fontSize: "1.2rem",
    lineHeight: "1.6rem",
    letterSpacing: "0.02rem",
    color: "#000000",
  },

  MuiChip: {
    fontFamily: "Inter",
    fontSize: "1rem",
    fontWeight: 500,
    lineHeight: "1.2rem",
    color: "#061136",
  },
};
